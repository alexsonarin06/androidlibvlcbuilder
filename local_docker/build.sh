#!/bin/bash
IMAGE_NAME=libvlc_android_build
CONTAINER_NAME=tmp_container_libvlc_android_build

docker build . -t $IMAGE_NAME

echo "Create temporary container"
docker run --name="$CONTAINER_NAME" $IMAGE_NAME /bin/true

echo "Copy artifacts"
docker cp $CONTAINER_NAME:/home/videolan/vlc-android/libvlc/build/outputs/aar/ ./

echo "Delete temporary container"
docker container rm $CONTAINER_NAME
